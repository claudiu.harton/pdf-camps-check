const page = document.querySelector(".container .row");

const generateCard = (camp) => {
  let availablility = "";

  if (camp.hasAvailableSlots && camp.isAvailableToRegister) {
    availablility = "Has available slots";
  } else if (!camp.hasAvailableSlots && !camp.isAvailableToRegister) {
    availablility = `Doesn't have available slots`;
  }

  return `<div class="col-md-3">
<div class="card-sl">
    <div class="card-image">
        <img
            src="${camp.imageUrl}" />
    </div>

    
    <div class="card-heading">
    ${camp.title}
    </div>
    <div class="card-text">
    ${camp.isAvailableToRegister ? "OPEN" : `CLOSED`}
    </div>
    <div class="card-text">
    ${availablility}
    </div>
    <a href="${camp.url}" target="_blank" class="card-button ${
    camp.isAvailableToRegister ? "green" : "red"
  }"> Visit page</a>
</div>
</div>`;
};

axios.get("./api/camps").then((response) => {
  const string = response.data.map((camp) => generateCard(camp)).join("");
  page.innerHTML = string;
});
