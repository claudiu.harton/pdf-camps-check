const axios = require("axios");
const cheerio = require("cheerio");
const express = require("express");
const cron = require("node-cron");
const SibApiV3Sdk = require("sib-api-v3-sdk");
const fs = require("fs");
const fetch = (...args) =>
  import("node-fetch").then(({ default: fetch }) => fetch(...args));
const secrets = require("./secrets");

const url = "https://www.pdf.ro/ro/cae/";
const PORT = process.env.PORT || 3030;
const summerCamps = "tabere-de-vara/";
const winterCamps = "tabere-de-schi/";

const myCustomLevels = {
  levels: {
    error: 0,
    info: 1,
    verbose: 3,
  },
  colors: {
    error: "red",
    info: "green",
    verbose: "magenta",
  },
};
// winston.remove(winston.transports.Console);
const winston = require("winston");
const { to, bcc, selectedCamp } = require("./properties.json");
const { createLogger, format, transports } = winston;
const { combine, colorize, timestamp, label, printf } = format;
winston.addColors(myCustomLevels.colors);
winston.format.combine(
  winston.format.colorize()
  // winston.format.json()
);
const myFormat = printf(({ level, message, timestamp }) => {
  return `${timestamp} [${level}]: ${message}`;
});

const logger = createLogger({
  levels: myCustomLevels.levels,
  format: winston.format.combine(
    timestamp(),
    winston.format.colorize({ all: true }),
    myFormat
  ),
  transports: [
    new winston.transports.Console({ level: "verbose" }),
    // new winston.transports.Console({ level: "update", colorize: true }),
  ],
});
const defaultClient = SibApiV3Sdk.ApiClient.instance;

const apiKey = defaultClient.authentications["api-key"];
apiKey.apiKey = secrets.apiKey;
const apiInstance = new SibApiV3Sdk.TransactionalEmailsApi();

const generateEmailContent = ({ title, url, imageUrl }) => {
  let html = fs.readFileSync(require.resolve("./email-html.html")).toString();

  html = html.replace("[TITLE]", title);
  html = html.replace("[IMAGE]", imageUrl);
  html = html.replace("[URL]", url);
  return {
    sender: { email: "claudiu.harton@gmail.com", name: "PDF Camps Check" },

    to: to,
    bcc: bcc.length > 0 ? bcc : undefined,
    subject: `S-a eliberat loc la ${title}`,
    htmlContent: html,
    headers: {
      "X-Mailin-custom":
        "custom_header_1:custom_value_1|custom_header_2:custom_value_2",
    },
  };
};

const hasAvailableSlotsCheck = async (url) => {
  const res = await axios.get(url);
  const html = res.data;
  const $ = cheerio.load(html);
  const userAgentRegExp = new RegExp(
    /NE PARE RĂU DAR NU MAI SUNT LOCURI DISPONIBILE PENTRU ACEST EVENIMENT/g
  );

  const userAgentRegExpResult = userAgentRegExp.exec($.text());

  const hasAvailableSlots = !userAgentRegExpResult;
  const isAvailableToRegister = !$(".pdf-enfold-event-page-online-reg")
    .attr("class")
    .includes("pdf-btn-disabled");
  return { hasAvailableSlots, isAvailableToRegister };
};

const getAvailableCamps = async (campType) => {
  const res = await axios.get(url + campType);
  const availableCamps = [];
  const html = res.data;
  const $ = cheerio.load(html);
  let id = 1;
  const statsTable = $(".pdf-category-event");
  statsTable.each(function () {
    const url = $(this).attr("href");
    const title = $(this).find("img").attr("title");
    const imageUrl = $(this).find("img").attr("src");
    availableCamps.push({ url, title, imageUrl, id: id++ });
  });
  return availableCamps;
};

const checkAvailability = async () => {
  const campsRaw = [
    ...(await getAvailableCamps(summerCamps)),
    ...(await getAvailableCamps(winterCamps)),
  ];
  const camps = await Promise.all(
    campsRaw.map(async (camp) => {
      const { hasAvailableSlots, isAvailableToRegister } =
        await hasAvailableSlotsCheck(camp.url);
      return {
        ...camp,
        hasAvailableSlots,
        isAvailableToRegister,
      };
    })
  );
  return camps;
};

let data = [];

const app = express();

app.use("/", express.static("public"));
app.get("/api/camps", (req, res) => {
  res.send(
    data.sort((a, b) => {
      return b.isAvailableToRegister != a.isAvailableToRegister
        ? b.isAvailableToRegister - a.isAvailableToRegister
        : a.hasAvailableSlots - b.hasAvailableSlots;
    })
  );
});

let availableCampsForRegister = 0;

let emailSent = false;

const sendEmails = async (desiredCamp) => {
  try {
    await apiInstance.sendTransacEmail(generateEmailContent(desiredCamp));
    logger.log(
      "verbose",
      `Email sent succesfully to: ${to.map((item) => item.email).join(", ")}${
        bcc.length ? "; bcc:" : ""
      } ${bcc.map((item) => item.email).join(", ")}`
    );
    logger.log("info", "-".repeat(75));
    emailSent = true;
  } catch (e) {
    logger.log("error", e.message);
  }
};

const sendSMS = async () => {
  try {
    const res = await fetch("https://api.msg91.com/api/v5/flow/", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        authkey: secrets.smsKey,
      },
      body: JSON.stringify({
        flow_id: "644fb903d6fc057218591262",
        sender: "pdfCamps",
        recipients: [...to, ...bcc]
          .filter((item) => item.phone)
          .map((item) => ({ mobiles: `4${item.phone}`, url: selectedCamp })),
      }),
    });
    if (res.ok) {
      logger.log(
        "verbose",
        `SMS sent succesfully to: ${[...to, ...bcc]
          .filter((item) => item.phone)
          .map((item) => item.phone)
          .join(", ")}`
      );
    } else {
      logger.log(
        "error",
        `SMS sent failed to: ${[...to, ...bcc]
          .filter((item) => item.phone)
          .map((item) => item.phone)
          .join(", ")}`
      );
    }
  } catch (e) {
    logger.log("error", e.message);
  }
};

const checkDesiredCamps = async (list) => {
  try {
    const newAvailableCampsForRegister = list.reduce(
      (a, b) => (b.isAvailableToRegister ? a + 1 : a),
      0
    );
    data = list;
    const desiredCamp = list.find((item) => item.url === selectedCamp);

    if (!desiredCamp.isAvailableToRegister) emailSent = false;

    if (!emailSent && desiredCamp.isAvailableToRegister) {
      logger.log(
        "verbose",
        `Available spot at ${desiredCamp.title} Sending notifications to recipiants...`
      );
      await Promise.all([sendEmails(desiredCamp), sendSMS()]);
    }

    if (newAvailableCampsForRegister != availableCampsForRegister) {
      availableCampsForRegister = newAvailableCampsForRegister;
      logger.log(
        "verbose",
        `List is updated:  ${availableCampsForRegister} available camps for register`
      );

      list
        .filter((item) => item.isAvailableToRegister)
        .map((item) => item.title)
        .sort((a, b) => a.localeCompare(b, "en", { numeric: true }))

        .forEach((item) => logger.log("verbose", item));
      logger.log("info", "-".repeat(75));
    } else {
      logger.log(
        "info",
        `No change in list:  ${availableCampsForRegister} available camps for register`,
        list
      );

      list
        .filter((item) => item.isAvailableToRegister)
        .map((item) => item.title)
        .sort((a, b) => a.localeCompare(b, "en", { numeric: true }))
        .forEach((item) => logger.log("info", item));
      logger.log("info", "-".repeat(75));
    }
  } catch (e) {
    logger.log("error", e.message);
  }
};

checkAvailability().then(checkDesiredCamps);

cron.schedule("* * * * *", function () {
  checkAvailability().then(checkDesiredCamps);
});

app.listen(PORT, () => {
  logger.log("info", `Server is running on http://localhost:${PORT}`);
});

// module.exports = app;
